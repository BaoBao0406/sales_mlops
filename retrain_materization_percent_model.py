#! python3
# retrain_materization_percent_model.py

import pandas as pd
import numpy as np
import re

from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
import category_encoders as ce
from sklearn.model_selection import train_test_split

import lightgbm as lgbm
from lightgbm import LGBMClassifier
from hyperopt import fmin, tpe, hp, anneal, Trials
from hyperopt import STATUS_OK
from hyperopt.pyll import scope
from sklearn.metrics import precision_recall_fscore_support as score


# preprocess data and retrain transformer
def preprocess_mat_percent_training_data(BK_ml_data):
    BK_ml_data = BK_ml_data[['Property', 'Account: Region', 'Account: Industry', 'Agency', 'End User Region', 'End User SIC', 'Booking Type', 'Blended Roomnights', 'Blended Guestroom Revenue Total', 
                                'Blended F&B Revenue', 'Blended Rental Revenue', 'Blended AV Revenue', 'Attendance', 'RSO Manager', 'Market Segment', 'Promotion', 'Blended ADR', 'Peak Roomnights Blocked', 
                                'Inhouse day', 'Lead day', 'Decision day', 'Arrival Month_sin', 'Booked Month_sin', 'Last Status Month_sin', 'Status']]
    
    X_percent = BK_ml_data.loc[:, BK_ml_data.columns != 'Status']
    y_percent = BK_ml_data['Status']
    
    columns_to_standarize_percent = ['Blended Roomnights', 'Blended Guestroom Revenue Total', 'Blended F&B Revenue', 'Blended Rental Revenue',
                             'Blended AV Revenue', 'Attendance', 'Blended ADR', 'Peak Roomnights Blocked']
    columns_to_te_percent = ['Property', 'Account: Region', 'Account: Industry', 'End User Region', 'End User SIC', 'Booking Type', 'Market Segment']
    
    X_train_percent, X_test_percent, y_train_percent, y_test_percent = train_test_split(X_percent, y_percent, test_size=0.2, shuffle=True)
    
    transformers_percent = [('standarize', StandardScaler(), columns_to_standarize_percent),
                            ('te', ce.TargetEncoder(), columns_to_te_percent)]
    ct_percent = ColumnTransformer(transformers_percent, remainder='passthrough')
    
    X_train_percent = ct_percent.fit_transform(X_train_percent, y_train_percent)
    X_test_percent = ct_percent.transform(X_test_percent)
    
    column_name = ['Agency', 'RSO Manager', 'Promotion', 'Inhouse day', 'Lead day', 'Decision day', 'Arrival Month_sin', 'Booked Month_sin', 'Last Status Month_sin']
    columns_name = columns_to_standarize_percent + list(ct_percent.named_transformers_['te'].get_feature_names()) + column_name
    
    X_train_percent = pd.DataFrame(X_train_percent, columns=columns_name).rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))
    X_test_percent = pd.DataFrame(X_test_percent, columns=columns_name).rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))
    
    return ct_percent, X_train_percent, X_test_percent, y_train_percent, y_test_percent


# Train materization percent model using lgbm
def hpyeropt_lgbm_classifier(X_train_percent, X_test_percent, y_train_percent, y_test_percent, N_FOLDS):
    # number of iterations
    max_evals = 100
    # count iterations
    count = 0
    
    lgb_train_dateset_percent = lgbm.Dataset(X_train_percent, label=y_train_percent)
    
    def objective(params, n_folds = N_FOLDS):
        global count
        
        params = {
                  'n_estimators': int(params['n_estimators']), 
                  'max_depth': int(params['max_depth']), 
                  'learning_rate': params['learning_rate'],
                  'num_leaves': int(params['num_leaves']),
                  'boosting_type': params['boosting_type'],
                  'colsample_bytree': params['colsample_bytree'],
                  'min_data_in_leaf': int(params['min_data_in_leaf']),
                  'subsample': params['subsample'],
                  'num_iterations': int(params['num_iterations']),
                  'feature_pre_filter': False,
                 }
        
        cv_results = lgbm.cv(params, lgb_train_dateset_percent, nfold = n_folds, num_boost_round = 1000, metrics = 'auc', 
                             stratified=True, eval_train_metric=True)
        best_score = max(cv_results['valid auc-mean'])
        loss = 1 - best_score
        # For creating train-test iteration score table
        max_index = np.argmax(cv_results['valid auc-mean'])
        best_train_score = cv_results['train auc-mean'][max_index]
        best_train_score = 1 - best_train_score
        count += 1
        
        return {'loss': loss, 'params': params, 'status': STATUS_OK,
                'score': {'count': [count], 'train auc-mean': [best_train_score], 'valid auc-mean': [loss]}}
    
    
    space={
       'n_estimators': scope.int(hp.quniform('n_estimators', 200, 700, 1)),
       'max_depth' : hp.quniform('max_depth', 5, 100, 1),
       'learning_rate': hp.loguniform('learning_rate', np.log(0.001), np.log(1)),
       'num_leaves': scope.int(hp.quniform('num_leaves', 5, 50, 1)),
       'boosting_type': hp.choice('boosting_type', ['gbdt', 'dart', 'goss']),
       'colsample_bytree': hp.uniform('colsample_bytree', 0.6, 1.0),
       'min_data_in_leaf': scope.int(hp.quniform('min_data_in_leaf', 200, 1000, 10)),
       'subsample': hp.uniform('subsample', 0.6, 1.0),
       'num_iterations': hp.quniform('num_iterations', 400, 1100, 10),
       'feature_pre_filter': False,
      }

    
    trials = Trials()
    # best parameters
    best = fmin(fn=objective, space=space, algo=tpe.suggest, max_evals=max_evals, trials=trials)
    # Using best parameters to fit into model and get prediction score
    model_percent = LGBMClassifier(n_estimators=int(best['n_estimators']), max_depth=int(best['max_depth']), boosting_type='dart', learning_rate=best['learning_rate'], num_iterations=int(best['num_iterations']),
                                   num_leaves=int(best['num_leaves']), colsample_bytree=best['colsample_bytree'], min_data_in_leaf=int(best['min_data_in_leaf']), subsample=float(best['subsample']), num_boost_round= 1000)
    model_percent.fit(X_train_percent, y_train_percent)
    y_pred_percent = model_percent.predict(X_test_percent)
    # get Overall model score
    precision, recall, fscore, _ = score(y_test_percent, y_pred_percent)
    model_score = pd.DataFrame([precision, recall, fscore], index=['precision', 'recall', 'fscore'], columns=['TD', 'D'])
    # get iteration score
    iteration_score = pd.DataFrame()
    for i in range(max_evals):
        tmp = pd.DataFrame(trials.trials[i]['result']['score'])
        iteration_score = iteration_score.append(tmp)
    
    return model_percent, model_score, best, iteration_score


# main function for retrain_materization_percent_model
def retrain_materization_percent_model(BK_ml_data):
    
    ct_percent, X_train_percent, X_test_percent, y_train_percent, y_test_percent = preprocess_mat_percent_training_data(BK_ml_data)
    
    N_FOLDS = 10
    
    model_percent, model_score, best, iteration_score = hpyeropt_lgbm_classifier(X_train_percent, X_test_percent, y_train_percent, y_test_percent, N_FOLDS)
    
    return ct_percent, model_percent, model_score, best, iteration_score
    
    
