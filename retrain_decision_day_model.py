#! python3
# retrain_decision_day_model.py

import pandas as pd
import numpy as np
import re, math

from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
import category_encoders as ce
from sklearn.model_selection import train_test_split

import xgboost as xgb
from xgboost import XGBRegressor
from hyperopt import fmin, tpe, hp, anneal, Trials
from hyperopt import STATUS_OK
from hyperopt.pyll import scope
from sklearn.metrics import mean_absolute_error, mean_squared_error


# preprocess data and retrain transformer
def preprocess_decision_day_training_data(BK_ml_data):
    BK_ml_data = BK_ml_data[['Property', 'Account: Region', 'Account: Industry', 'Agency', 'End User Region', 'End User SIC', 'Booking Type', 'Blended Roomnights', 'Blended Guestroom Revenue Total', 
                                'Blended F&B Revenue', 'Blended Rental Revenue', 'Blended AV Revenue', 'Attendance', 'RSO Manager', 'Market Segment', 'Promotion', 'Blended ADR', 'Peak Roomnights Blocked', 
                                'Inhouse day', 'Lead day', 'Decision day', 'Arrival Month_sin', 'Booked Month_sin', 'Last Status Month_sin']]
    
    X_decision = BK_ml_data.loc[:, BK_ml_data.columns != 'Decision day']
    y_decision = BK_ml_data['Decision day']
    
    columns_to_standarize_decision = ['Blended Roomnights', 'Blended Guestroom Revenue Total', 'Blended F&B Revenue', 'Blended Rental Revenue',
                             'Blended AV Revenue', 'Attendance', 'Blended ADR', 'Peak Roomnights Blocked']
    columns_to_te_decision = ['Property', 'Account: Region', 'Account: Industry', 'End User Region', 'End User SIC', 'Booking Type', 'Market Segment']
    
    X_train_decision, X_test_decision, y_train_decision, y_test_decision = train_test_split(X_decision, y_decision, test_size=0.2, shuffle=True)
    
    transformers_decision = [('standarize', StandardScaler(), columns_to_standarize_decision),
                             ('te', ce.TargetEncoder(), columns_to_te_decision)]
    ct_decision = ColumnTransformer(transformers_decision, remainder='passthrough')
    
    X_train_decision = ct_decision.fit_transform(X_train_decision, y_train_decision)
    X_test_decision = ct_decision.transform(X_test_decision)
    
    column_name = ['Agency', 'RSO Manager', 'Promotion', 'Inhouse day', 'Lead day', 'Arrival Month_sin','Booked Month_sin', 'Last Status Month_sin']
    columns_name = columns_to_standarize_decision + list(ct_decision.named_transformers_['te'].get_feature_names()) + column_name
    
    X_train_decision = pd.DataFrame(X_train_decision, columns=columns_name).rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))
    X_test_decision = pd.DataFrame(X_test_decision, columns=columns_name).rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))
    
    return ct_decision, X_train_decision, X_test_decision, y_train_decision, y_test_decision


# Train decision day model using xgboost
def hpyeropt_xgboost_regressor(X_train_decision, X_test_decision, y_train_decision, y_test_decision, N_FOLDS):
    # number of iterations
    max_evals = 100
    # count iterations
    count = 0
    
    xgb_train_dateset_decision = xgb.DMatrix(X_train_decision, label=y_train_decision)
    
    def objective(params, n_folds = N_FOLDS):
        global count
        
        params = {
                  'num_boost_round': int(params['num_boost_round']), 
                  'max_depth': int(params['max_depth']), 
                  'learning_rate': params['learning_rate'],
                  'gamma': params['gamma'],
                  'booster': params['booster'],
                  'colsample_bytree': params['colsample_bytree'],
                  'colsample_bylevel': params['colsample_bylevel'],
                  'subsample': params['subsample'],
                  'min_child_weight': int(params['min_child_weight']),
                  'lambda': params['lambda'],
                  'alpha': params['alpha'],
                  'reg_lambda': params['reg_lambda'],
                  'reg_alpha': params['reg_alpha']
                 }
        
        cv_results = xgb.cv(params, xgb_train_dateset_decision, nfold = n_folds, metrics = 'rmse', stratified=True)
        loss = min(cv_results['test-rmse-mean'])
        # For creating train-test iteration score table
        max_index = cv_results['test-rmse-mean'].idxmax()
        best_train_score = cv_results.loc[max_index, 'train-rmse-mean']
        count += 1
        
        return {'loss': loss, 'params': params, 'status': STATUS_OK,
                'score': {'count': [count], 'train-rmse-mean': [best_train_score], 'test-rmse-mean': [loss]}}
    
    space={
       'num_boost_round': hp.quniform('num_boost_round', 50, 1000, 1),
       'max_depth' : hp.quniform('max_depth', 2, 70, 1),
       'learning_rate': hp.loguniform('learning_rate', np.log(0.001), np.log(1)),
       'gamma': hp.uniform('gamma', 0.1, 1.0),
       'booster': hp.choice('booster', ['gblinear', 'dart', 'gbtree']),
       'colsample_bytree': hp.uniform('colsample_bytree', 0.4, 1.0),
       'colsample_bylevel': hp.uniform('colsample_bylevel', 0.4, 1.0),
       'subsample': hp.uniform('subsample', 0.4, 1.0),
       'min_child_weight': hp.quniform('min_child_weight', 5, 20, 1),
       'lambda': hp.quniform('lambda', 1, 10, 0.2),
       'alpha': hp.quniform('alpha', 1, 10, 0.2),
       'reg_lambda': hp.quniform('reg_lambda', 0.0, 10, 0.2),
       'reg_alpha': hp.quniform('reg_alpha', 1, 20, 0.2),
      }

    trials = Trials()
    # best parameters
    best = fmin(fn=objective, space=space, algo=tpe.suggest, max_evals=max_evals, trials=trials)
    # Using best parameters to fit into model and get prediction score
    model_decision = XGBRegressor(max_depth=int(best['max_depth']), booster='gbtree', learning_rate=best['learning_rate'], alpha=best['alpha'], colsample_bylevel=best['colsample_bylevel'],
                                  gamma=int(best['gamma']), colsample_bytree=best['colsample_bytree'], min_child_weight=int(best['min_child_weight']), subsample=float(best['subsample']), num_boost_round=best['num_boost_round'],
                                  reg_alpha=best['reg_alpha'], reg_lambda=best['reg_lambda'])
    model_decision.fit(X_train_decision, y_train_decision)
    y_pred_decision = model_decision.predict(X_test_decision)
    # get Overall model score
    mse = (mean_squared_error(y_test_decision, y_pred_decision))
    mae = (mean_absolute_error(y_test_decision, y_pred_decision))
    rmse = math.sqrt(mean_squared_error(y_test_decision, y_pred_decision))
    model_score = pd.DataFrame([mse, mae, rmse], index=['MSE', 'MAE', 'RMSE'], columns=['score'])
    # get iteration score
    iteration_score = pd.DataFrame()
    for i in range(max_evals):
        tmp = pd.DataFrame(trials.trials[i]['result']['score'])
        iteration_score = iteration_score.append(tmp)
    
    return model_decision, model_score, best, iteration_score


# main function for retrain_decision_day_model
def retrain_decision_day_model(BK_ml_data):
    
    ct_decision, X_train_decision, X_test_decision, y_train_decision, y_test_decision = preprocess_decision_day_training_data(BK_ml_data)
    
    N_FOLDS = 10
    
    model_decision, model_score, best, iteration_score = hpyeropt_xgboost_regressor(X_train_decision, X_test_decision, y_train_decision, y_test_decision, N_FOLDS)
    
    return model_decision, model_score, best, iteration_score
