#! python3
# sales_mlops_metadata.py - 

import pickle, os
from datetime import date, datetime


# Save transformer and model
def save_transformer_model(metadata_file_path, model_name, transformer, model):
    # Save ML model
    pkl_filename = metadata_file_path + model_name + '_ml.pkl'
    with open(pkl_filename, 'wb') as file:
        pickle.dump(model, file)
    
    # Save Transformer
    tf_filename = metadata_file_path + model_name + '_tf.pkl'
    with open(tf_filename, 'wb') as file:
        pickle.dump(transformer, file)


# Save metadata into csv
def save_metadata(metadata_file_path, model_name, model_score, params):
    metadata_filename = metadata_file_path + model_name + '_metadata.csv'
    with open(metadata_filename, 'w') as file:
        file.write('Model Name: \n' + model_name + '\n\n')
        file.write('Overall Model Score: \n')
        model_score_string = model_score.to_string(header=True, index=True)
        file.write(model_score_string + '\n\n')
        file.write('Best Parameter: \n' + str(params))


# Save train-test iteration score
def save_iteration_score(metadata_file_path, model_name, iteration_score):
    score_filename = metadata_file_path + model_name + '_iteration_score.csv'
    iteration_score.to_csv(score_filename)
    

# main function for sales_mlops_metadata
def sales_mlops_metadata(path, model_name, transformer, model, model_score, params, iteration_score):
    
    # metadata file path
    now = datetime.now()
    metadata_file_path = path + '\\metadata\\'
    metadata_file_path = metadata_file_path + str(now.year) + '_' + str(now.month) + '_' + str(now.day) + '\\'
    
    if not os.path.exists(metadata_file_path):
        os.makedirs(metadata_file_path)
    
    # run function to save transformer and model
    save_transformer_model(metadata_file_path, model_name, transformer, model)
    # run function to save meatadata
    save_metadata(metadata_file_path, model_name, model_score, params)
    # run function to save iteration score
    save_iteration_score(metadata_file_path, model_name, iteration_score)
    
