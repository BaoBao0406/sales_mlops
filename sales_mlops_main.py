#! python3
# sales_mlops_main.py - 

import re, math, pickle, os
from datetime import date, datetime
import pandas as pd
import numpy as np
from dateutil.relativedelta import relativedelta
from sklearn.metrics import precision_recall_fscore_support as score
from sklearn.metrics import mean_absolute_error, mean_squared_error

import sales_mlops_sql_data, retrain_materization_percent_model, retrain_decision_day_model, sales_mlops_metadata


# ML prediction - materization percent
def materization_percent_model_score(transformer, model, BK_ml_data):
    BK_ml_tmp = BK_ml_data[['Property', 'Account: Region', 'Account: Industry', 'Agency', 'End User Region', 'End User SIC', 'Booking Type', 'Blended Roomnights', 'Blended Guestroom Revenue Total', 
                                'Blended F&B Revenue', 'Blended Rental Revenue', 'Blended AV Revenue', 'Attendance', 'RSO Manager', 'Market Segment', 'Promotion', 'Blended ADR', 'Peak Roomnights Blocked', 
                                'Inhouse day', 'Lead day', 'Decision day', 'Arrival Month_sin', 'Booked Month_sin', 'Last Status Month_sin']]
    columns_to_standarize_percent = ['Blended Roomnights', 'Blended Guestroom Revenue Total', 'Blended F&B Revenue', 'Blended Rental Revenue',
                                     'Blended AV Revenue', 'Attendance', 'Blended ADR', 'Peak Roomnights Blocked']
    columns_to_percent = ['Property', 'Account: Region', 'Account: Industry', 'End User Region', 'End User SIC', 'Booking Type', 'Market Segment']
    column_normal_percent = ['Agency', 'RSO Manager', 'Promotion', 'Inhouse day', 'Lead day', 'Decision day', 'Arrival Month_sin',
                             'Booked Month_sin', 'Last Status Month_sin']
    
    column_name_percent = columns_to_standarize_percent + columns_to_percent + column_normal_percent
    
    BK_ml_tf = transformer.transform(BK_ml_tmp)
    BK_ml_tf = pd.DataFrame(BK_ml_tf, columns=column_name_percent)
    BK_ml_tf = BK_ml_tf.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))
    BK_ml_tf = BK_ml_tf.astype(float)
    y_pred_tf = model.predict_proba(BK_ml_tf)
    # prediction materization percent
    y_pred_tf = pd.DataFrame(y_pred_tf, columns = ['TD %', 'D %'])
    y_pred_tf.reset_index(drop=True, inplace=True)
    
    BK_ml_pred_data = BK_ml_data[['BK_no']]
    BK_ml_pred_data.reset_index(drop=True, inplace=True)
    BK_ml_pred_data = pd.concat([BK_ml_pred_data, y_pred_tf], axis=1)
    
    # calculate model score
    precision, recall, fscore, _ = score(BK_ml_pred_data['Status'], BK_ml_pred_data['D %'])
    materization_score = pd.DataFrame([precision, recall, fscore], index=['precision', 'recall', 'fscore'], columns=['TD', 'D'])
    
    return materization_score
    

# ML prediction - decision day
def decision_day_model_score(transformer, model, BK_ml_data):
    BK_ml_tmp = BK_ml_data[['Property', 'Account: Region', 'Account: Industry', 'Agency', 'End User Region', 'End User SIC', 'Booking Type', 'Blended Roomnights', 'Blended Guestroom Revenue Total', 
                                 'Blended F&B Revenue', 'Blended Rental Revenue', 'Blended AV Revenue', 'Attendance', 'RSO Manager', 'Market Segment', 'Promotion', 'Blended ADR', 'Peak Roomnights Blocked', 
                                 'Inhouse day', 'Lead day', 'Arrival Month_sin', 'Booked Month_sin']]
    columns_to_standarize_decision = ['Blended Roomnights', 'Blended Guestroom Revenue Total', 'Blended F&B Revenue', 'Blended Rental Revenue',
                             'Blended AV Revenue', 'Attendance', 'Blended ADR', 'Peak Roomnights Blocked']
    columns_to_te_decision = ['Property', 'Account: Region', 'Account: Industry', 'End User Region', 'End User SIC', 'Booking Type', 'Market Segment']
    column_normal_decision = ['Agency', 'RSO Manager', 'Promotion', 'Inhouse day', 'Lead day', 'Arrival Month_sin', 'Booked Month_sin']
    
    column_name_decision = columns_to_standarize_decision + columns_to_te_decision + column_normal_decision
    
    BK_ml_tf = transformer.transform(BK_ml_tmp)
    BK_ml_tf = pd.DataFrame(BK_ml_tf, columns=column_name_decision)
    BK_ml_tf = BK_ml_tf.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))
    BK_ml_tf = BK_ml_tf.astype(float)
    y_pred_tf = model.predict(BK_ml_tf)
    # prediction decision day
    y_pred_tf = pd.DataFrame(y_pred_tf, columns = ['Decision day predict'])
    y_pred_tf.reset_index(drop=True, inplace=True)
    
    # Join decision day and materization percent
    BK_ml_pred_data = BK_ml_data[['BK_no']]
    BK_ml_pred_data.reset_index(drop=True, inplace=True)
    BK_ml_pred_data = pd.concat([BK_ml_pred_data, y_pred_tf], axis=1)
    
    # calculate model score
    mse = (mean_squared_error(BK_ml_pred_data['Decision day'], BK_ml_pred_data['Decision day predict']))
    mae = (mean_absolute_error(BK_ml_pred_data['Decision day'], BK_ml_pred_data['Decision day predict']))
    rmse = math.sqrt(mean_squared_error(BK_ml_pred_data['Decision day'], BK_ml_pred_data['Decision day predict']))
    decision_day_score = pd.DataFrame([mse, mae, rmse], index=['MSE', 'MAE', 'RMSE'], columns=['score'])

    return decision_day_score


# main function for sales_mlops_main
def main():
    # Load Transformer and prediction model - materization percent
    path = os.path.abspath(os.getcwd())
    
    Transformer_percent = open(path + 'Materization_percent_tf.pkl', 'rb')
    Transformer_percent = pickle.load(Transformer_percent)
    
    Model_percent = open(path + 'Materization_percent_ml.pkl', 'rb')
    Model_percent = pickle.load(Model_percent)
    
    # Load Transformer and prediction model - decision day
    Transformer_decision = open(path + 'decision_day_tf.pkl', 'rb')
    Transformer_decision = pickle.load(Transformer_decision)
    
    Model_decision = open(path + 'decision_day_ml.pkl', 'rb')
    Model_decision = pickle.load(Model_decision)

    # Get all data data from amadeus
    amadeus_data = sales_mlops_sql_data.mlops_data_for_checking()
    
    materization_score = materization_percent_model_score(Transformer_percent, Model_percent, amadeus_data)
    
    decision_day_score = decision_day_model_score(Transformer_decision, Model_decision, amadeus_data)
    
    # if either materization_score or decision_day_score is below target score, then retrain model
    if ((materization_score.loc['fscore', 'TD'] < 0.82) | (materization_score.loc['fscore', 'D'] < 0.82)) | (decision_day_score.loc['RMSE', 'score'] > 42):
        # Merge amadeus data and delphi952 history data
        BK_ml_data = sales_mlops_sql_data.mlops_data_for_retrain(path, amadeus_data)
        
        if (materization_score.loc['fscore', 'TD'] < 0.82) | (materization_score.loc['fscore', 'D'] < 0.82):
            ct_percent, model_percent, model_score_percent, best_percent, iteration_score_percent = retrain_materization_percent_model.retrain_materization_percent_model(BK_ml_data)
            
            sales_mlops_metadata.sales_mlops_metadata(path, 'materization_percent', ct_percent, model_percent, model_score_percent, best_percent, iteration_score_percent)
            
        if decision_day_score.loc['RMSE', 'score'] > 42:
            ct_decision, model_decision, model_score_decision, best_decision, iteration_score_decision = retrain_decision_day_model.retrain_decision_day_model(BK_ml_data)
            
            sales_mlops_metadata.sales_mlops_metadata(path, 'decision_day', ct_decision, model_decision, model_score_decision, best_decision, iteration_score_decision)
