#! python3
# sales_mlops_sql_data_.py - 

import pyodbc
from datetime import date, datetime
import pandas as pd
import numpy as np
from dateutil.relativedelta import relativedelta



def user_data(conn):
    # FDC User ID and Name list
    user = pd.read_sql('SELECT DISTINCT(Id), Name \
                        FROM dbo.[User]', conn)
    user = user.set_index('Id')['Name'].to_dict()
    
    return user


# ML prediction
def amadeus_sql_data(conn, user, start_date, end_date):
    amadeus_bk_data = pd.read_sql("SELECT BK.Id, BK.Booking_ID_Number__c, FORMAT(BK.nihrm__ArrivalDate__c, 'yyyy-MM-dd') AS ArrivalDate, FORMAT(BK.nihrm__DepartureDate__c, 'yyyy-MM-dd') AS DepartureDate, BK.nihrm__CommissionPercentage__c, BK.Percentage_of_Attrition__c, BK.nihrm__Property__c, BK.nihrm__FoodBeverageMinimum__c, ac.Name AS ACName, ag.Name AS AGName, BK.End_User_Region__c, \
                                    BK.End_User_SIC__c, BK.nihrm__BookingTypeName__c, BK.RSO_Manager__c, BK.Non_Compete_Clause__c, ac.nihrm__RegionName__c, ac.Industry, BK.nihrm__CurrentBlendedRoomnightsTotal__c, BK.nihrm__BlendedGuestroomRevenueTotal__c, BK.VCL_Blended_F_B_Revenue__c, BK.nihrm__CurrentBlendedEventRevenue7__c, BK.nihrm__CurrentBlendedEventRevenue4__c, \
                                    BK.nihrm__BookingMarketSegmentName__c, BK.Promotion__c, BK.nihrm__CurrentBlendedADR__c, BK.nihrm__PeakRoomnightsBlocked__c, FORMAT(BK.nihrm__BookedDate__c, 'yyyy-MM-dd') AS BookedDate, FORMAT(BK.nihrm__LastStatusDate__c, 'yyyy-MM-dd') AS LastStatusDate, BK.nihrm__BookingStatus__c \
                             FROM dbo.nihrm__Booking__c AS BK \
                                    LEFT JOIN dbo.Account AS ac \
                                    ON BK.nihrm__Account__c = ac.Id \
                             LEFT JOIN dbo.Account AS ag \
                                    ON BK.nihrm__Agency__c = ag.Id \
                             WHERE (BK.nihrm__BookingTypeName__c NOT IN ('ALT Alternative', 'CN Concert', 'IN Internal')) AND \
                                   (BK.nihrm__Property__c NOT IN ('Sands Macao Hotel')) AND (BK.nihrm__BookingStatus__c IN ('Definite', 'TurnedDown'))", conn)
    amadeus_bk_data['RSO_Manager__c'].replace(user, inplace=True)
    amadeus_bk_data.columns = ['Id', 'BK_no', 'ArrivalDate', 'DepartureDate', 'Commission', 'Attrition', 'Property', 'F&B Minimum', 'Account', 'Agency', 'End User Region',
                         'End User SIC', 'Booking Type', 'RSO Manager', 'Non-compete', 'Account: Region', 'Account: Industry', 'Blended Roomnights', 'Blended Guestroom Revenue Total',
                         'Blended F&B Revenue', 'Blended Rental Revenue', 'Blended AV Revenue', 'Market Segment', 'Promotion', 'Blended ADR', 'Peak Roomnights Blocked', 
                         'BookedDate', 'LastStatusDate', 'Status']
    
    amadeus_ev_data = pd.read_sql("SELECT ET.nihrm__Booking__c, ET.nihrm__Property__c, MAX(ET.nihrm__AgreedAttendance__c) \
                                FROM dbo.nihrm__BookingEvent__c AS ET \
                                WHERE (ET.nihrm__Property__c NOT IN ('Sands Macao Hotel')) AND (ET.nihrm__BookedDate__c BETWEEN CONVERT(datetime, '" + start_date + "') AND CONVERT(datetime, '" + end_date + "')) \
                                GROUP BY ET.nihrm__Booking__c, ET.nihrm__Property__c", conn)
    amadeus_ev_data.columns = ['Id', 'property', 'Attendance']
    amadeus_ev_data = amadeus_ev_data[['Id', 'Attendance']]
    amadeus_data = amadeus_bk_data.join(amadeus_ev_data.set_index('Id'), on='Id')
    
    return amadeus_data


# preprocess delphi952 old data
def delphi952_old_data(path):
    delphi952_old_data_path = path + '\\delpli952_data.xls'
    
    delphi952_data = pd.read_excel(delphi952_old_data_path, sheet_name='Old', dayfirst=True)
    
    delphi952_data = delphi952_data.drop(delphi952_data.columns[-5:], axis=1)
    
    delphi952_column = ['Property', 'Account: Region', 'Account: Industry', 'Agency', 'SIC', 'BookedDate', 'Status', 'ArrivalDate', 'DepartureDate', 'End User Region', 'End User SIC', 'Booking Type', 'LastStatusDate', 'Blended Roomnights',
                        'Blended Guestroom Revenue Total', 'Blended F&B Revenue', 'Blended Rental Revenue', 'Blended AV Revenue', 'Attendance', 'RSO Manager', 'Market Segment', 'Promotion', 'Blended ADR', 'Peak Roomnights Blocked',
                        'Account', 'BK_no']
    
    delphi952_data.columns = delphi952_column
    
    delphi952_data['BookedDate'] = pd.to_datetime(delphi952_data['BookedDate']).dt.date
    delphi952_data['ArrivalDate'] = pd.to_datetime(delphi952_data['ArrivalDate'])
    delphi952_data['DepartureDate'] = pd.to_datetime(delphi952_data['DepartureDate'])
    delphi952_data['LastStatusDate'] = pd.to_datetime(delphi952_data['LastStatusDate'])
    
    
    PROP = {'VMRH': 'The Venetian Macao', 'CMCC': 'Conrad Macao Cotai Strip', 'HICC': 'Holiday Inn Macao Cotai Central'}
    delphi952_data['Property'] = delphi952_data['Property'].replace(PROP)
    
    STAT = {'DEF': 'Definite', 'TURN': 'TurnedDown', 'CAN': 'Cancelled'}
    delphi952_data['Status'] = delphi952_data['Status'].replace(STAT)
    
    TYPE = {'GE': 'GE Group Exhibition/Trade Show', 'EX': 'EX Expo/Exhibition', 'GQ': 'GQ Group Corp Social',
            'GR': 'GR Group Corporate Residential', 'TS': 'GE Group Exhibition/Trade Show', 'CC': 'CC Catering - Corporate',
            'GI': 'GI Group Incentive', 'GS': 'GS Group Tour Series', 'CS': 'CS Catering - Social', 'GC': 'GC Group Convention',
            'GT': 'GT Group Tour One Time', 'GN': 'GN Group Corp Non-Res (Rm Only)', 'GA': 'GA Group Ad Hoc Leisure',
            'CN': 'CN Concert', '-': np.nan, 'GRP': np.nan}
    
    delphi952_data['Booking Type'] = delphi952_data['Booking Type'].replace(TYPE)
    
    delphi952_data['Account: Region'].replace('-', np.nan, inplace=True)
    delphi952_data['End User Region'].replace('-', np.nan, inplace=True)
    delphi952_data['Promotion'].replace('-', np.nan, inplace=True)
    
    acc_industry = {'Animal Feed': 'Agriculture/Animal related', 'Veterinary Services': 'Agriculture/Animal related', 'Wood and Lumber': 'Agriculture/Animal related', 'Agriculture': 'Agriculture/Animal related',
               'Car & Motor Vehicle Rental': 'Automotive', 'Air Transportation/Airlines': 'Aviation', 'Aviation & Aerospace': 'Aviation', 'Architecture & Design': 'Building & Construction',
               'Administration': 'Business Orgs/Services', 'Business Associations/Organizations': 'Business Orgs/Services', 'Business Services': 'Business Orgs/Services', 'Computer Software': 'Computers, Information Tech', 
               'Internet': 'Computers, Information Tech', 'Personal Service': 'Consulting Services', 'Semiconductor': 'Electronics', 'Entertainment': 'Entertainment/Recreation',
               'Tools & Instruments': 'Engineering', 'Fishing, Hunting & Trapping': 'Entertainment/Recreation', 'Museums, Art Galleries': 'Entertainment/Recreation', 'Telecommunications': 'Computers, Information Tech',
               'Photography': 'Entertainment/Recreation', 'Sporting Goods & Recreation': 'Entertainment/Recreation', 'Toys & Hobby Goods': 'Entertainment/Recreation', 'Chemical & Allied Products': 'Agriculture/Animal related', 
               'Event & Promotional Organization': 'Event Planners', 'Incentive': 'Event Planners', 'Food Service & Restaurants': 'Food & Beverage', 'Media-TV, Radio, Print, Motion Pic, Vide': 'Media',
               'Embassy & Diplomatic Services': 'Government', 'Dental Equipment & Supplies': 'Healthcare', 'Laboratory': 'Healthcare', 'Optical': 'Healthcare', 'Non-Durable Goods, Wholesale': 'Retail Sales - Other',
               'Industrial Materials/Manufacture': 'Manufacturing', 'Packaging': 'Manufacturing', 'Paper Products': 'Manufacturing', 'Textile Manufacturing': 'Manufacturing', 
               'Advertising/Marketing': 'Media', 'Publishing': 'Media', 'Direct Marketing': 'Multi Level Marketing', 'Cleaning & Maintenance': 'Nonclassifiable Establishments',
               'Alternative Energy': 'Oil, Gas, Petroleum & Energy', 'Heating & Air Conditioning': 'Oil, Gas, Petroleum & Energy', 'Utilities-Gas,Water,Light': 'Oil, Gas, Petroleum & Energy',
               'Apparel & Retail Sale': 'Retail Sales - Other', 'Social & Civic, Fraternal Org': 'Social Organisations', 'Social Svcs, Childcare': 'Social Organisations', 'Durable Goods,Wholesale': 'Retail Sales - Luxury',
               'Cruise Lines': 'Transportation', 'Distribution Services': 'Transportation', 'Express Delivery, Courier Svc., Mailings': 'Transportation', 'Import/Export': 'Transportation',
               'Logistics Company': 'Transportation', 'Motor Vehicles & Equipment': 'Transportation', 'Public Transport': 'Transportation', 'Railroad Transportation': 'Transportation', 'Media-TV,Radio,Print,Motion Pic,Video': 'Media',
               'Footwear': 'Retail Sales - Luxury', 'Furniture, Fixtures, Int. Design': 'Retail Sales - Luxury', 'Groceries & Related Products': 'Retail Sales - Luxury',  'Watches/Jewelry': 'Retail Sales - Luxury',
               'Shipping Company': 'Transportation', 'Tour Operators': 'Travel Agencies', 'Wholesale': 'Travel Agencies', 'Safety/Security': 'Security', 'Garment Industry & Manufacturing': 'Manufacturing', np.nan: '-'}
    
    delphi952_data['Account: Industry'] = delphi952_data['Account: Industry'].replace(acc_industry)
    delphi952_data['End User SIC'] = delphi952_data['End User SIC'].replace(acc_industry)
    
    SIC = {'Business & Professional Org':  'Business Orgs/Services', 'Cleaning/Maintenance': 'Nonclassifiable Establishments',  'Nonclassifiable Establishment': 'Nonclassifiable Establishments',
               'Durable Goods, Wholesale': 'Retail Sales - Luxury',  'Embassy & Diplomatic Svcs': 'Government', 'Event & Promotional Org': 'Event Planners',  'Express Delivery/Courier Svc/Mailings': 'Transportation',
               'Express Delivery/CourierSvc/Mailings': 'Transportation',  'Fishing, Hunting, Trapping': 'Entertainment/Recreation',  'Furniture, Fixtures, Int Design': 'Retail Sales - Luxury', 
               'Furnitures, Fixtures, Int Design': 'Retail Sales - Luxury',  'Garment Industry Mfr': 'Manufacturing',  'Industrial Materials Mfr': 'Manufacturing',  'Medical/Hospital': 'Healthcare',
               'Motor Vehicles Equipment': 'Transportation',  'Oil, Gas, Petroleum, Energy': 'Oil, Gas, Petroleum & Energy', 'Pest Control': 'Nonclassifiable Establishments',  'Retail & Department Stores':  'Retail Sales - Other',
               'Utilities-Gas, Water, Light': 'Oil, Gas, Petroleum & Energy',  'Wood & Lumber': 'Agriculture/Animal related', 'zzThird Party': 'Travel Agencies'}
    
    delphi952_data['End User SIC'] = delphi952_data['End User SIC'].replace(SIC)
    
    delphi952_data.drop(delphi952_data.loc[delphi952_data['End User SIC']=='Internal Account'].index, inplace=True)
    delphi952_data.drop(delphi952_data.loc[delphi952_data['Account: Industry']=='Internal Account'].index, inplace=True)
    delphi952_data.drop(delphi952_data.loc[delphi952_data['Booking Type']=='IN Internal'].index, inplace=True)
    delphi952_data.drop(delphi952_data.loc[delphi952_data['Booking Type']=='ALT Alternative'].index, inplace=True)
    
    delphi952_data[['Blended Rental Revenue', 'Blended AV Revenue', 'Blended ADR', 'Attendance']].fillna(value=0, inplace=True)
    delphi952_data['End User SIC'] = np.where(delphi952_data['End User SIC'].isna(), delphi952_data['Account: Industry'], delphi952_data['End User SIC'])
    delphi952_data['End User Region'] = np.where(delphi952_data['End User Region'].isna(), delphi952_data['Account: Region'], delphi952_data['End User Region'])
    delphi952_data['Account: Region'] = np.where(delphi952_data['Account: Region'].isna(), delphi952_data['End User Region'], delphi952_data['Account: Region'])
    
    return delphi952_data


# Merge delphi952 to FDC data
def merge_delphi952_amadeus_data(amadeus_data, delphi952_data):
    ml_training_columns = ['Property', 'Account: Region', 'Account: Industry', 'Agency', 'End User Region', 'End User SIC', 'Booking Type', 'Blended Roomnights',
                           'Blended Guestroom Revenue Total', 'Blended F&B Revenue', 'Blended Rental Revenue', 'Blended AV Revenue', 'Attendance',
                           'RSO Manager', 'Market Segment', 'Promotion', 'Blended ADR', 'Peak Roomnights Blocked', 'ArrivalDate', 'DepartureDate', 
                           'BookedDate', 'LastStatusDate', 'Status', 'BK_no']
    
    amadeus_data = amadeus_data[ml_training_columns]
    delphi952_data = delphi952_data[ml_training_columns]
    
    data = [delphi952_data, amadeus_data]
    BK_merge_data = pd.concat(data)
    BK_merge_data = BK_merge_data.reset_index()
    BK_merge_data = BK_merge_data.drop(['index'], axis=1)
    
    return BK_merge_data


# ML prediction data preprocessing
def preprocessing_merge_data(BK_ml_data):
    BK_ml_data = BK_ml_data[['Property', 'Account: Region', 'Account: Industry', 'Agency', 'End User Region', 'End User SIC', 'Booking Type', 'Blended Roomnights',
                            'Blended Guestroom Revenue Total', 'Blended F&B Revenue', 'Blended Rental Revenue', 'Blended AV Revenue', 'Attendance',
                            'RSO Manager', 'Market Segment', 'Promotion', 'Blended ADR', 'Peak Roomnights Blocked', 'ArrivalDate', 'DepartureDate', 
                            'BookedDate', 'LastStatusDate', 'Status']]
    # calculate Inhouse day (Departure - Arrival)    
    BK_ml_data['Inhouse day'] = (pd.to_datetime(BK_ml_data['DepartureDate']).dt.date - pd.to_datetime(BK_ml_data['ArrivalDate']).dt.date).dt.days
    # calculate Lead day (Arrival - Booked) 
    BK_ml_data['Lead day'] = (pd.to_datetime(BK_ml_data['ArrivalDate']).dt.date - pd.to_datetime(BK_ml_data['BookedDate']).dt.date).dt.days
    # calculate Decision day (Last Status date - Booked) 
    BK_ml_data['Decision day'] = (pd.to_datetime(BK_ml_data['LastStatusDate']).dt.date - pd.to_datetime(BK_ml_data['BookedDate']).dt.date).dt.days
    # booking info Arrival Month
    BK_ml_data['Arrival Month'] = pd.DatetimeIndex(BK_ml_data['ArrivalDate']).month
    BK_ml_data['Arrival Month_sin'] = np.sin(2 * np.pi * BK_ml_data['Arrival Month']/12)
    # booking info Booked Month
    BK_ml_data['Booked Month'] = pd.DatetimeIndex(BK_ml_data['BookedDate']).month
    BK_ml_data['Booked Month_sin'] = np.sin(2 * np.pi * BK_ml_data['Booked Month']/12)
    # booking info Last Status Month
    BK_ml_data['Last Status Month'] = pd.DatetimeIndex(BK_ml_data['LastStatusDate']).month
    BK_ml_data['Last Status Month_sin'] = np.sin(2 * np.pi * BK_ml_data['Last Status Month']/12)
    # convert text column to boolean
    BK_ml_data['Agency'] = BK_ml_data['Agency'].apply(lambda x: 0 if x is np.nan else 1)
    BK_ml_data['RSO Manager'] = BK_ml_data['RSO Manager'].apply(lambda x: 0 if x is np.nan else 1)
    BK_ml_data['Promotion'] = BK_ml_data['Promotion'].apply(lambda x: 0 if x is np.nan else 1)
    
    BK_ml_data = BK_ml_data.drop(['BookedDate', 'ArrivalDate', 'DepartureDate', 'LastStatusDate'], axis =1)
    BK_ml_data['Blended ADR'].replace(np.nan, 0, inplace=True)
    BK_ml_data['Peak Roomnights Blocked'].replace(np.nan, 0, inplace=True)
    BK_ml_data.dropna(subset=['Booking Type'], inplace=True)
    BK_ml_data['Status'] = BK_ml_data['Status'].apply(lambda x: 1 if x == 'Definite' else 0)
    
    # eliminate negative value for lead day and decision day
    mean = BK_ml_data['Lead day'].mean()
    BK_ml_data['Lead day'].loc[BK_ml_data['Lead day'] < 0] = mean
        
    return BK_ml_data


# main function for checking mlops data
def mlops_data_for_checking():
    # Set date range
    now = datetime.now()
#    start_year_booked = now - relativedelta(years=3)
    end_year_booked = now + relativedelta(years=5)
    start_date_booked = '2016-01-01'
    end_date_booked = str(end_year_booked.year) + '-12-31'
    
    
    conn = pyodbc.connect('Driver={SQL Server};'
                          'Server=VOPPSCLDBN01\VOPPSCLDBI01;'
                          'Database=SalesForce;'
                          'Trusted_Connection=yes;')
    
    user = user_data(conn)
    
    amadeus_data = amadeus_sql_data(conn, user, start_date_booked, end_date_booked)
    
    amadeus_data = preprocessing_merge_data(amadeus_data)
    
    return amadeus_data


# main function for retrain mlops data
def mlops_data_for_retrain(path, amadeus_data):
    
    delphi952_data = delphi952_old_data(path)
    
    BK_merge_data = merge_delphi952_amadeus_data(amadeus_data, delphi952_data)
    
    BK_ml_data = preprocessing_merge_data(BK_merge_data)
    
    return BK_ml_data
